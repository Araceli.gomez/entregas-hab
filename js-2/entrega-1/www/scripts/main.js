import {crearReservaHotel, eliminarHotel, eliminarTodo} from './utils.js'

const listadoHoteles = Array.from(document.querySelectorAll('.boton_reserva'));

const hotelesSeleccionados = document.getElementById('hoteles_seleccionados');

const botonEliminarTodo = document.getElementById('EliminarTodo')

listadoHoteles.forEach( BotonHotel => {
    BotonHotel.addEventListener('click',e => {
         crearReservaHotel(e.target.parentNode.parentNode)
        })
})

hotelesSeleccionados.addEventListener('click', e => {
    e.stopPropagation()
    if (e.target.classList= 'eliminar_hotel_seleccionado') { 
        eliminarHotel(e.target.parentNode.parentNode.parentNode)
    }
} )

botonEliminarTodo.addEventListener('click', () => eliminarTodo())