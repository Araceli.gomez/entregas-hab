const TableHotelesSeleccionados = document.getElementById('hoteles_seleccionados');
const hotelesSeleccionados = document.getElementById('hoteles_seleccionados');
const panelReservas = document.getElementById('panelReservas')

const crearObjetoReservaHotel = tarjetaHotel => {
    return  {
        nombreHotel: tarjetaHotel.querySelector('.h1_card').textContent,
        imagen: tarjetaHotel.querySelector('.card_imagen').firstChild.src,
        precioNoche: tarjetaHotel.querySelector('.precio_hotel').textContent
    }
    
}

const crearElemento = inforHotel => {

    const tarjetaHotel_ContenedorFilaImagen = document.createElement('tr');
    const tarjetaHotel_ContenedorColumnaImagen = document.createElement('td');
    const tarjetaHotel_Imagen = document.createElement('img');
    tarjetaHotel_ContenedorColumnaImagen.rowSpan= 4; 
    tarjetaHotel_ContenedorColumnaImagen.classList = 'imagenHotelSeleccionado';
    tarjetaHotel_Imagen.src=inforHotel.imagen;
    tarjetaHotel_Imagen.alt=inforHotel.nombreHotel;
    
    tarjetaHotel_ContenedorFilaImagen.appendChild(tarjetaHotel_ContenedorColumnaImagen)
    tarjetaHotel_ContenedorColumnaImagen.appendChild(tarjetaHotel_Imagen)
    
    const tarjetaHotel_ContenedorFija = document.createElement('tr');
    const tarjetaHotel_FilaNombre = document.createElement('td');
    tarjetaHotel_FilaNombre.colSpan = 2;
    tarjetaHotel_FilaNombre.textContent= inforHotel.nombreHotel
    tarjetaHotel_ContenedorFija.appendChild(tarjetaHotel_FilaNombre)
    
    const tarjetaHotel_columnaPrecio = document.createElement('tr');
    const tarjetaHotel_FilaPrecio = document.createElement('td');
    tarjetaHotel_FilaPrecio.colSpan = 2;
    tarjetaHotel_FilaPrecio.textContent= inforHotel.precioNoche;
    tarjetaHotel_columnaPrecio.appendChild(tarjetaHotel_FilaPrecio)

    const tarjetaHotel_ColumnaBotones = document.createElement('tr');
    const tarjetaHotel_FilaBotonEliminar = document.createElement('td');
    const tarjetaHotel_FilaBotonReserva =  document.createElement('td');
    const tarjetaHotel_botonEliminar = document.createElement('button');
    tarjetaHotel_botonEliminar.textContent = 'x';
    tarjetaHotel_botonEliminar.classList = 'eliminar_hotel_seleccionado';
    tarjetaHotel_FilaBotonEliminar.appendChild(tarjetaHotel_botonEliminar)
    tarjetaHotel_FilaBotonReserva.textContent = 'Reservar'
    tarjetaHotel_ColumnaBotones.appendChild(tarjetaHotel_FilaBotonEliminar)
    tarjetaHotel_ColumnaBotones.appendChild(tarjetaHotel_FilaBotonReserva)

    const tablaTarjetaHotel = document.createElement('table');
    tablaTarjetaHotel.id=inforHotel.nombreHotel;

    tablaTarjetaHotel.appendChild(tarjetaHotel_ContenedorFilaImagen)
    tablaTarjetaHotel.appendChild(tarjetaHotel_ContenedorFija)
    tablaTarjetaHotel.appendChild(tarjetaHotel_columnaPrecio)
    tablaTarjetaHotel.appendChild(tarjetaHotel_ColumnaBotones)

    hoteles_seleccionados.appendChild(tablaTarjetaHotel)
}

const obtenerLocalStorage  = () => {
    return JSON.parse(localStorage.getItem('hotelesSeleccionados'))
}

const addLocalStorage = hotel => {
    const hotelesYaSeleccionados =obtenerLocalStorage() || []
    const NuevoshotelSelecionados =  [...hotelesYaSeleccionados, hotel]
    console.log(NuevoshotelSelecionados)
    localStorage.setItem('hotelesSeleccionados',JSON.stringify(NuevoshotelSelecionados))
}

export const crearReservaHotel = HotelSeleccionado => {
    let objetoHotel = []
    objetoHotel = crearObjetoReservaHotel(HotelSeleccionado)
    panelReservas.classList = 'listadoCompraSeleccionado';
    crearElemento(objetoHotel)
    addLocalStorage(objetoHotel)
     
}

export const eliminarHotel = HotelSeleccionado => {
    hotelesSeleccionados.removeChild(HotelSeleccionado)
    if (hotelesSeleccionados.childNodes.length==1)  {
        panelReservas.classList = 'listadoCompra';
    }
}


export const eliminarTodo = () => {
     hotelesSeleccionados.innerHTML= '';
     panelReservas.classList = 'listadoCompra';
}