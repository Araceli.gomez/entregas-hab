

let productos = []
let id = 0

const annadirProducto =  (req, res) => {
    const {name, stock, precio} = req.body

    if (!name || !stock || !precio) {
        res.status(409).send('Debe indicar todos los parámetros')
        return
    }
    if (stock <0){
        res.status(409).send('El stock debe de ser positivo')
        return
    }
    if (precio <0){
        res.status(409).send('El precio debe de ser positivo')
        return
    }
    const existeProducto = productos.filter(producto => producto.name === name)
    if (existeProducto.length>0){
        res.status(409).send('El producto ya existe')
        return
    }
    id ++;
    productos.push({
        id: id,
        name,
        stock, 
        precio
    })
    console.log(productos)
    res.send()
}

const modificarProductoCompleto = (req, res) => {
    const {id, name, stock, precio} = req.body
    
    if (!name || !stock || !precio || !id) {
        res.status(409).send('Debe indicar todos los parámetros')
        return
    }
    if (stock <0){
        res.status(409).send('El stock debe de ser positivo')
        return
    }
    if (precio <0){
        res.status(409).send('El precio debe de ser positivo')
        return
    }
    
    const existeProducto = productos.filter(producto => producto.id === parseInt(id))
    if (existeProducto.length!==1){
        res.status(409).send('El producto no existe')
        return
    }
    
    productos = productos.map(producto =>{
        if (producto.id === parseInt(id)){
            
            producto.name =  name, 
            producto.stock =  stock,
            producto.precio = precio

        }
        return producto
    })
    res.send()
}

const modificarParametroProducto = (req, res) => {
    const {id, name, stock, precio} = req.body
    if (!id){
        res.status(409).send('Debe de indicar el id del producto a modificar')
        return
    }
    if (!name && !stock && !precio) {
        res.status(409).send('Debe de indicar al menos un campo a modificar.')
    }
    if (stock && stock <0){
        res.status(409).send('El stock debe de ser superior a 0')
        return
    }
    if (precio && precio <0){
        res.status(409).send('El precio debe de ser superior a 0')
        return
    }
        
    const existeProducto = productos.filter(producto => producto.id === parseInt(id))
    if (existeProducto.length!==1){
        res.status(409).send('El producto no existe')
        return
    }
    productos = productos.map(producto =>{
        if (producto.id === parseInt(id)){
            
            producto.name =  name || producto.name, 
            producto.stock =  stock || producto.stock,
            producto.precio = precio || producto.precio

        }
        return producto
    })
    console.log(productos)
    res.send()
}

const eliminarProducto = (req, reso) => {
    const {id} = req.body
    if (!id){
        res.status(409).send('Debe de indicar el id del producto a eliminar')
        return
    }
    const longitudInicial = productos.length
    productos = productos.filter(producto => producto.id != parseInt(id))
    if (longitudInicial > productos.length){
        res.send()
        return
    }
    res.status(409).send('El id no existe')
}

const listarProductos = (req, res) => {
    let productosFiltrados =  productos
 
    if (Object.values(req.query).length>0){
        productosFiltrados = productosFiltrados.filter(producto =>{
            return ((!req.query['name'] || producto.name === req.query['name'])  &&
                (!req.query['stock'] || producto.stock === req.query['stock']) &&
                (!req.query['precio'] || producto.precio === req.query['precio']))                
        })
    }
    res.json(productosFiltrados)
}


 

 module.exports = {
    annadirProducto, 
    modificarProductoCompleto, 
    modificarParametroProducto, 
    eliminarProducto,
    listarProductos
 }