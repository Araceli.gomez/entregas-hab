'use strict '

console.log('------------------------------------------------- CREAR UN ARRAY CON LOS TWEETS DISTINTOS----------------------------------')

let tweets = ['aprendiendo #javascript en vigo #HBOS.#PHPFFF #DDD #java #ddd',
   'empezando el sagundo modulo de bootcamp #javascript.#hbos #jj'];


let hashtag = '';
let posicionFinHastag = 0;
let caracteresMarcanFinHastag = ' .,¿?#'
let hastagArray = []
let hastagRepetido = false
for (tweet of tweets) {
    hashtag = tweet.slice(tweet.indexOf('#'));
   
    while (hashtag.indexOf('#')>-1){
        posicionFinHastag =hashtag.length;
        hashtag = hashtag.slice(hashtag.indexOf('#'))
        for (let i=0; i<caracteresMarcanFinHastag.length;i++) {
            if (hashtag.indexOf(caracteresMarcanFinHastag[i])>0 && hashtag.indexOf(caracteresMarcanFinHastag[i]) < posicionFinHastag ){
                posicionFinHastag = hashtag.indexOf(caracteresMarcanFinHastag[i]);
                
            }
        }
        hastagRepetido = false
        for (let hastagReconocido of hastagArray){
            if (hastagReconocido.toUpperCase() == hashtag.slice(1,posicionFinHastag).toUpperCase()){
                hastagRepetido = true;
            }
        }
        if (hastagRepetido==false){
            hastagArray.push(hashtag.slice(1,posicionFinHastag).toUpperCase())
        }
        hashtag = hashtag.slice(hashtag.indexOf('#')+posicionFinHastag+1,hashtag.length)
    }
}
console.log(`Los hastag son: ${hastagArray}`);