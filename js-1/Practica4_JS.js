'use strict'

const axios = require('axios')
/**
 * El objetivo de este ejercicio es crear una estructura de datos
 * que contenga los datos de:
 *    - pacientes hospitalizados
 *    - pacientes en UCI
 *    - pacientes dados de alta
 *    - casos totales
 * 
 * Como resultados tendrás una estructura de la siguiente forma:
 * {
 *     <nombre comunidad autónoma>: {
 *         'fecha en formato AAAA-MM-DD': {
 *             hospitalizados: xx,
 *             uci: xx,
 *             altas: xx,
 *             casos: xx
 *         }
 *     },
 *    <nombre comunidad autónoma 2> : {...}
 *    ....
 * }
 * 
 * Crea una función para bajar los datos y almacenarlos en la estructura
 * con el formato indicado.
 * 
 * Crea, además, funciones para:
 *    - devolver el estado de la pandemia para un día. Esta función recibirá una fecha
 *      en formato AAAA-MM-DD y devolverá una estructura:
 *        {
 *            <nombre comunidad autónoma>: {
 *                hospitalizados: xx, 
 *                uci: xx, 
 *                altas: xx, 
 *                casos: xx
 *            }
 *        }
 * 
 * 
 * URLS:
 *    - hospitalizados: https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_hospitalizados.csv
 *    - uci: https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_uci.csv
 *    - altas: https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_altas.csv
 *    - casos: https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_casos.csv
 * 
 */

 async function descargaArchivos(){
     const URL_HOSPITALIZADOS = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_hospitalizados.csv';
     const URL_UCI = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_uci.csv';
     const URL_ALTAS = ' https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_altas.csv';
     const URL_CASOS = 'https://raw.githubusercontent.com/datadista/datasets/master/COVID%2019/ccaa_covid19_casos.csv';

    let hospitalizados = axios.get(URL_HOSPITALIZADOS);
    let uci = axios.get(URL_UCI);
    let altas = axios.get(URL_ALTAS);
    let casos = axios.get(URL_CASOS);

    const archivosCovidDescargados = await Promise.all([hospitalizados,uci,altas,casos]);
    
    return archivosCovidDescargados.map(archivo => archivo.data)

 }
function formatearArchivoEnArray(archivo){
    const eliminarLineasBlanco = (linea => linea.split(',')[0].length>1 )
    return archivo
        .split('\n')
        .filter(eliminarLineasBlanco)
        .slice(0,-1)
}

function crearDiccionario(archivo){
    const hospitalizados = formatearArchivoEnArray(archivo[0])
    const uci = formatearArchivoEnArray(archivo[1])
    const altas = formatearArchivoEnArray(archivo[2])
    const casos = formatearArchivoEnArray(archivo[3])
    let  diccionario = {}
    // cambiar el ejercici para comprobar fechas
    let fechas = hospitalizados[0].split(',').slice(2)
    for (let i=1; i< hospitalizados.length-1;i++){
        let comunidad = hospitalizados[i].split(',')[1]
        let datosFecha = {}
        for (let j=0;j<fechas.length;j++){  
            datosFecha[fechas[j]] = {
                hospitalizados: hospitalizados[i].split(',')[j+2],
                uci: uci[i].split(',')[j+2],
                altas: altas[i].split(',')[j+2],
                casos: casos[i].split(',')[j+2],
            }
           
        }
        diccionario[comunidad] = datosFecha
    }
   return diccionario
}

estadoPandemiaDia(diccionario, fecha) = ((diccionario, fecha) =>  Object.entries(diccionario).map(comunidad => Object.assign({},{[comunidad[0]] :  comunidad[1][fecha]})))
        
const dia = '2020-04-15'
let idInterval
 descargaArchivos().then(archivo => {
    clearInterval(idInterval)
    const diccionario = crearDiccionario(archivo)
    console.log('Estado en dia', estadoPandemiaDia(diccionario,dia))
    
})

 let contadorAste=1
const TIEMPOESPERA = 1000;
console.log(`Descargando archivos`)
idInterval = setInterval(function(){
    console.log(`${'*'.repeat(contadorAste)}`);
    contadorAste++;
},TIEMPOESPERA) 