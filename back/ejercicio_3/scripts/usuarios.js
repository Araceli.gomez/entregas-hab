const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
//contraseña: 1234
const usuarios = [{
    email: 'ara@mail.com',
    contrasennaEncriptada: '$2b$10$CsEZsxog.YURx1kQgvGL5uFeRegmTz0uQWwfE5SzgOOKq0SMs3tr6',
    rol: 'admin'
}]


const registro = async (req, res) => {
    const { email, contrasenna, rol } = req.body;
    if (!email || !contrasenna) {
        res.status(400).send('Debe rellenar todos los campos');
        return;
    }
    const existeUsuario = usuarios.filter(usuario => usuario.email === email)
    if (existeUsuario.length>0) {
        res.status(409).send('El usuario ya existe');
        return;
    }
    // si pasa validaciones encriptar constraseña. Es asíncrona 
    const contrasennaEncriptada = await bcrypt.hash(contrasenna, 10);
    usuarios.push({
        email,
        contrasennaEncriptada, 
        rol
    })
    res.send('Usuario creado con éxito')
}



const login =  async (req, res) => {
    // buscar si existe el mail 
    const {email, contrasenna, rol} = req.body;


    if (!email|| !contrasenna){
        res.status(409).send('Debe indicar el mail')
        return
    }

    
    const usuarioLogeado = usuarios.filter(usuario => usuario.email === email)
    
   

    if (usuarioLogeado.length<=0){
        res.status(401).send('Usuario no registrado')
        return
    }
    const contrasennavalida = await bcrypt.compare(contrasenna, usuarioLogeado[0].contrasennaEncriptada);
    if (!contrasennavalida) {
        res.status(401).send('Contraseña incorrecta');
        return
    }
    
    const tokenPayload = { id: usuarioLogeado[0].email, role: usuarioLogeado[0].rol };
    const token = jwt.sign(tokenPayload, process.env.SECRET, {
        expiresIn: '1d'
    });
 
    res.json({
        token
    })
    
}

const usuarioLogeado = (req, res, next) => {
    const { authorization } = req.headers;
    
    try {
        if (!authorization) {
            
            return next({
                status: 401,
                message: 'usuario no logeado'}); 
        }
        const decodedToken = jwt.verify(authorization, process.env.SECRET);
        req.auth = decodedToken;
    } catch(e) {
       return next({
           status: '401',
           message: 'usuario no logeado'});
    }

    next();
}

const esAdmin = (req,res, next) =>{
    console.log(req.auth)
     if (req.auth.role  !== 'admin'){
         return next({
             status: '409',
             message: 'usuario no administrador'});
     }    
    next()
}

const listarUsuarios = (req, res) =>{
    res.json(usuarios)
}

module.exports = {
    login,
    registro, 
    listarUsuarios,
    usuarioLogeado,
    esAdmin
}
