'use strict'

const axios = require('axios');

// https://jsonplaceholder.typicode.com/posts
// https://jsonplaceholder.typicode.com/posts/1

// a) Generar contador de mensajes por usario
// b) Generar una lista con la siguiente estructura:
/*[
    {
        userId: <userId>,
        posts: [
            {
                title: <title>
                body: <body>     // hay que obtenerlo de la segunda petición
            },
            {
                title: <title>
                body: <body>
            },
        ]
    }

]
*/

let idInterval;

async function obtenerDatos(){
    const URLDATOSGENERALES = 'https://jsonplaceholder.typicode.com/posts';
  
    const devolverArrayId = (datos => datos.id);
    const concatenarURLdetalles = id => axios.get(`${URLDATOSGENERALES}/${id}`);
    let llamadas = await axios.get(URLDATOSGENERALES)
   
    llamadas = llamadas.data
          .map(devolverArrayId)
          .map(concatenarURLdetalles);

    const detalleEntradasForo = await Promise.all(llamadas);
 
    return detalleEntradasForo

    
}

function contadorMensajesUsuario(informacion){
    let mensajesPorUsuario= {};
   // hacer reduce
    const contarMensajesPorUsuario = mensaje => {
        if (mensajesPorUsuario[`Usuario ${mensaje.data.userId}`] == undefined) {
            mensajesPorUsuario[`Usuario ${mensaje.data.userId}`] = 1;
        } 
        else{
            mensajesPorUsuario[`Usuario ${mensaje.data.userId}`] ++;
        }
    
    }
    informacion.forEach(contarMensajesPorUsuario)
    return mensajesPorUsuario
     
}

function listarMensajesPorUsuario(informacion){
    let mensajesPorUsuario= [];
    let usuariosDetectados = [];
    for (let mensaje of informacion) {
        if (usuariosDetectados.indexOf(mensaje.data.userId)===-1){
            usuariosDetectados.push(mensaje.data.userId)
            mensajesPorUsuario.push({
                userId: mensaje.data.userId,
                post: [{
                    title: mensaje.data.title, 
                    body:mensaje.data.body}]
            })  
        }
        else {
    
            mensajesPorUsuario[usuariosDetectados.indexOf(mensaje.data.userId)]['post']
                .push({
                    title: mensaje.data.title, 
                    body:  mensaje.data.body
                })

        }
      
    }
   
    return mensajesPorUsuario
}

obtenerDatos().then(datos =>{
    clearInterval(idInterval);
    console.log(contadorMensajesUsuario(datos));
    const prueba =  listarMensajesPorUsuario(datos)
    console.log(Object.values(prueba))
});

let i=1
const TIEMPOESPERA = 500;
console.log(`Descargando archivos`)
idInterval = setInterval(function(){
    console.log(`${'*'.repeat(i)}`);
    i++;
},TIEMPOESPERA)