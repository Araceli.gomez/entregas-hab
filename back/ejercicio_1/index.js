'use strict'
const calculoCuota = require('amortize');
const parametros = require('commander');

const validacionImporte = (importe, ahorro, plazo, interes) => {
    let validacion = []
    if (isNaN(importe)) {
        validacion.push('Debe de introducir un importa valido');
    }
    else if (parseInt(importe) <= 0) {
        validacion.push('El importe debe de ser positivo')
    }
    if (isNaN(ahorro)) {
        validacion.push('El ahorro debe de ser positivo')
    }
    else if(parseInt(ahorro) < 0) {
        validacion.push('Debe indicar un número positivo en el ahorro')
    }
    if (parseInt(importe) <= parseInt(ahorro)) {
        validacion.push('El importe solicitado debe ser superior al ahorro')
    }
    if (isNaN(plazo)) {
        valicacion.push('El plazo debe de ser un número')
    }
    if (parseInt(plazo)<=0) {
        validacion.push('El plazo debe de ser un número positivo')
    }
    if (isNaN(interes)) {
        validacion.push('El interés debe de ser un número ')
    }
    if (parseInt(interes)<0) {
        validacion.push('El tipo de interés debe de ser un número positivo')}
    return validacion

}

parametros
    .option('-c, --importe <0>', 'importe')
    .option('-c --ahorro <0>', 'ahorro')
    .option('-c --plazo <0>', 'plazo')
    .option('-c --interes <0>', 'interes')
    .parse(process.argv)

//validación de parámetros

let validarParametros =  validacionImporte(parametros.importe, parametros.ahorro, parametros.plazo, parametros.interes)

if (validarParametros.length == 0)
console.log(`La cuota es: ${calculoCuota({
    amount: parseInt(parametros.importe) - parseInt(parametros.ahorro),
    rate: parseInt(parametros.interes),
    totalTerm: parseInt(parametros.plazo),
    amortizeTerm: parseInt(parametros.plazo)

  }).basePaymentRound}`)

else {
    console.log(validarParametros)
}
