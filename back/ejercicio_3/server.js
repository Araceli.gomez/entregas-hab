require('dotenv').config();

const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');


const port = process.env.PORT;

const {login, registro, listarUsuarios, esAdmin, usuarioLogeado} = require('./scripts/usuarios')

const {annadirProducto, modificarProductoCompleto, modificarParametroProducto, eliminarProducto, listarProductos } = require('./scripts/gestionProductos')

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());


// GESTIÓN DE USUARIOS

app.post('/user', registro);

app.post('/user/login', login);

app.get('/user/listado', usuarioLogeado,esAdmin,  listarUsuarios);

app.use((error, req, res, next) => {
    console.log('error')
    res
        .status(error.status || 500)
        .send({status: 'error', message: error.message})
    return
})


 app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});

// GESTIÓN DE PRODUCTOS


app.post('/product/add', usuarioLogeado, annadirProducto)

app.put('/product/put', usuarioLogeado, modificarProductoCompleto)

app.patch('/product/patch', usuarioLogeado, modificarParametroProducto)

app.delete('/product/delete', usuarioLogeado, eliminarProducto)



//LISTADO PRODUCTOS 
app.get('/product', listarProductos) 