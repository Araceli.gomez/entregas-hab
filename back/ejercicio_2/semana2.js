'use strict'
/**
 * Un cliente nos pide realizar un sistema para gestionar eventos culturales.
 * Necesita dar de alta eventos, que pueden ser de tipo 'concierto', 'teatro' o 
 * 'monólogo'. Cada uno se caracteriza por un 'nombre', 'aforo' y 'artista'.
 * Opcionalmente pueden incluir una descripción.
 * 
 * El cliente necesitará una API REST para añadir conciertos y poder obtener
 * una lista de los existentes.
 * 
 * El objetivo del ejercicio es que traduzcas estos requisitos a una descripción
 * técnica, esto es, decidir qué endpoints hacen falta, qué parámetros y cuáles 
 * son los código de error a devolver
 * 
 * Notas:
 *    - el conocimiento necesario para realizarlo es el adquirido hasta la clase del
 *      miércoles
 *    - llega con un endpoint GET y otro POST
 *    - el almacenamiento será en memoria, por tanto cuando se cierre el servidor
 *      se perderán los datos. De momento es aceptable esto.
 * 
 */

const server = require('express');
const bodyParser = require('body-parser');

let concierto = [];
let teatro = [];
let monologo = [];

const CATEGORIAS = {
    'CONCIERTO': 0,
    'TEATRO': 0,
    'MONOLOGO': 0
}
const app = server();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


function altaElemento(categoria, nombreCategoria, nombre, aforo, artista, descripcion) {
    let error = []
    let nuevoID = parseInt(CATEGORIAS[nombreCategoria]) + 1

    // comprobar información si está completa
    if (nombre == undefined || nombre == '') {
        error.push('Debe indicar el nombre')
    }
    if (aforo == undefined || aforo == '') {
        error.push('Debe indicar el aforo')
    }
    if (artista == undefined || artista == '') {
        error.push('Debe indicar el artista')
    }
    if (error != '') {
        return {
            'error': 400,
            'descripcion': error
        }
    }
    console.log(categoria, categoria.length)
    // comprobar si ya existe 
    if (categoria.length > 0) {
        const existeEvento = categoria.filter(evento => evento.nombre.toUpperCase() == nombre.toUpperCase())
        if (existeEvento.length > 0) {
            return {
                'error': 409,
                'descripcion': 'El evento ya existe'
            }
        }
    }
    // grabar informacion
    categoria.push({
        'id': nuevoID,
        'nombre': nombre.toUpperCase(),
        'aforo': aforo,
        'artista': artista.toUpperCase(),
        'descripcion': descripcion || ''
    })
    CATEGORIAS[nombreCategoria] = nuevoID;

    return {
        'error': 200,
        'descripcion': ''
    }
}

function eliminarElemento(categoria, idElemento) {
    let numeroEventosInicial = categoria.length
    categoria = categoria.filter(evento => evento.id != idElemento)
    console.log(categoria)

    if (numeroEventosInicial > categoria.length) {
        return {
            'error': 200,
            'descripcion': '',
            'datos': categoria
        }
    }
    return {
        'error': 400,
        'descripcion': 'El elemento a eliminar no existe',
        datos: categoria
    }
}

function modificarElemento(categoria, elemento, tipoModificacion) {
    let resultado = {
        error: 400,
        descripcion: 'Se ha producido un error',
        datos: categoria
    }
    let error = []
    // validar si es tipo de modificaciíon Put tenga todos los campos. 
    if (tipoModificacion == 'Push') {
        if (elemento.nombre == undefined || elemento.nombre == '') {
            error.push('Debe indicar el nombre')
        }
        if (elemento.aforo == undefined || elemento.aforo == '') {
            error.push('Debe indicar el aforo')
        }
        if (elemento.artista == undefined || elemento.artista == '') {
            error.push('Debe indicar el artista')
        }
        if (error != '') {
            return {
                'error': 400,
                'descripcion': error,
                datos: categoria
            }
        }
    }
     

    //validar si existe el nombre
    
    const existeEvento = categoria.filter(evento => evento.nombre.toUpperCase() == elemento.nombre.toUpperCase() 
                                        || evento.id != elemento.id)
    if (existeEvento.length <1 || categoria.length<=0) {
        return {
            'error': 409,
            'descripcion': 'El evento no existe',
            datos: categoria

        }
    }
    else if (existeEvento.length == 2) {
        return {
            'error': 409,
            'descripcion': 'El evento ya existe',
            datos: categoria
        }

    }

    categoria = categoria.map(evento => {
        if (evento.id == elemento.id) {
            resultado = {
                error: 200,
                descripcion: ''
            }
            if (tipoModificacion == 'patch') {
                elemento.nombre = elemento.nombre.toUpperCase() || evento.nombre;
                elemento.aforo = elemento.aforo || evento.aforo;
                elemento.artista = elemento.artista || evento.artista;
                elemento.descripcion = elemento.descripcion || evento.descripcion;
            }
            return elemento;
        }
        return evento
    })
    if (resultado.error == 200) {
        resultado = {
            error: 200,
            descripcion: '',
            datos: categoria
        }
    }
    return resultado
}

app.get('/:categoria', (req, res) => {
    if (Object.keys(CATEGORIAS).indexOf(req.params.categoria.toUpperCase()) == -1) {
        res.status(400).send('La categoria no existe')
    }
    else {
        switch (req.params.categoria.toUpperCase()) {
            case 'CONCIERTO':
                res.json(concierto);
                break;
            case 'TEATRO':
                res.json(teatro);
                break;
            case 'MONOLOGO':
                res.json(monologo);
                break;
        }
        res.json()
    }

});

app.post('/:categoria', (req, res) => {
    let resultado = {}
    switch (req.params.categoria.toUpperCase()) {
        case 'CONCIERTO':
            resultado = altaElemento(concierto, req.params.categoria.toUpperCase(), req.body.nombre, req.body.aforo, req.body.artista, req.body.descripcion)
            break;
        case 'TEATRO':
            resultado = altaElemento(teatro, req.params.categoria.toUpperCase(), req.body.nombre, req.body.aforo, req.body.artista, req.body.descripcion)
            break;
        case 'MONOLOGO':
            resultado = altaElemento(monologo, req.params.categoria.toUpperCase(), req.body.nombre, req.body.aforo, req.body.artista, req.body.descripcion)
            break;
        default:
            resultado = {
                error: 400,
                descripcion: 'La categoria no existe'
            };
            break;
    }
    res.status(resultado.error).send(resultado.descripcion)

})

app.delete('/:categoria/:id', (req, res) => {
    let resultado = {}
    switch (req.params.categoria.toUpperCase()) {
        case 'CONCIERTO':
            resultado = eliminarElemento(concierto, req.params.id);
            concierto = resultado.datos;
            break;
        case 'TEATRO':
            resultado = eliminarElemento(teatro, req.params.id);
            teatro = resultado.datos;
            break;
        case 'MONOLOGO':
            resultado = eliminarElemento(monologo, req.params.id);
            monologo = resultado.datos;
            break;
        default:
            resultado = {
                error: 400,
                descripcion: 'La categoria no existe'
            };
            break;
    }
    res.status(resultado.error).send(resultado.descripcion)

})

app.put('/:categoria/:id', (req, res) => {

    let elemento = {
        'id': req.params.id,
        'nombre': req.body.nombre.toUpperCase(),
        'aforo': req.body.aforo,
        'artista': req.body.artista,
        'descripcion': req.body.descripcion || ''
    }
    let resultado = {}
    switch (req.params.categoria.toUpperCase()) {
        case 'CONCIERTO':
            resultado = modificarElemento(concierto, elemento, 'put');
            concierto = resultado.datos;
            break;
        case 'TEATRO':
            resultado = modificarElemento(teatro, elemento, 'put');
            teatro = resultado.datos;
            break;
        case 'MONOLOGO':
            resultado = modificarElemento(monologo, elemento, 'put');
            monologo = resultado.datos;
            break;
        default:
            resultado = {
                error: 400,
                descripcion: 'La categoria no existe'
            };
            break;
    }
    res.status(resultado.error).send(resultado.descripcion)
})

app.patch('/:categoria/:id', (req, res) => {
    let elemento = {
        'id': req.params.id,
        'nombre': req.body.nombre.toUpperCase() || req.body.nombre,
        'aforo': req.body.aforo,
        'artista': req.body.artista,
        'descripcion': req.body.descripcion || ''
    }
    let resultado = {}
    switch (req.params.categoria.toUpperCase()) {
        case 'CONCIERTO':
            resultado = modificarElemento(concierto, elemento, 'patch');
            concierto = resultado.datos;
            break;
        case 'TEATRO':
            resultado = modificarElemento(teatro, elemento, 'patch');
            teatro = resultado.datos;
            break;
        case 'MONOLOGO':
            resultado = modificarElemento(monologo, elemento, 'patch');
            monologo = resultado.datos;
            break;
        default:
            resultado = {
                error: 400,
                descripcion: 'La categoria no existe'
            };
            break;
    }
    res.status(resultado.error).send(resultado.descripcion)
})

app.listen(3000) 
